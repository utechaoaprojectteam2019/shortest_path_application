# Shortest Paths Between Jamaican Towns Project 

This project is designed to test the ability of students taking this course, to understand a typical real-life problem and propose an efficient solution, decide on the computational means and design technique of their proposed solution, design the algorithm, prove its correctness, and analyze and implement it in accordance to what was taught in the course. 

It is becoming increasingly common to see several websites and mobile apps which show the shortest distances from one set of towns to another set. For example, an expandable overlaid map of Jamaica is displayed on the website [https://www.distancefromto.net/distance-between-jamaica-cities](https://www.distancefromto.net/distance-between-jamaica-cities) and it also shows the distances in both km and miles between 19 Jamaican towns. 

A similar website 
[http://members.tripod.com/~Livi_d/tourism/distance.htm](http://members.tripod.com/~Livi_d/tourism/distance.htm) shows the approximate distances between one set A of 51 Jamaican towns and another set B of 17 towns, where B∈A . This project requires you to use the most efficient algorithms and data structures you can find or can develop yourself to determine the shortest path between all 51 towns in set A and all other towns in A. 

## Required 

Write the code for an application that provides a solution to the problems posed above. Use any programming language of your choice and make use of the most efficient techniques, data structures, and algorithmic designs you have learned in this course, or can find through your own research. 

The output from your application should be a table showing the shortest distance from each town to all other towns in set A. 

Further, after the table is generated, if the user enters or selects one town from set A as source and any another town from the same set, your application should show the route from the source to the destination and the total path cost. 

Your completed project should summarise the problem in your own words, show the computational means and design technique you decided to use, show the design of the algorithm you used to solve the problem, prove the correctness of the algorithm, and analyse the algorithm to show how efficient and appropriate it is. You should also provide the source code and runtime executable of the application you develop. 

## Marking Scheme 

```html
Summarizing the problem and restating it in your own words ... 5% 
Computational means and design technique ... 5% 
Algorithmic design and description of how it works ... 10% 
Proof of correctness ... 5% 
Analysis of Algorithm ... 5% 
Correct Implementation ... 70% 
```

The project should be appropriately documented, including the design, implementation and sample runs. Your tutor will mark your project during the tutorial session. Extra marks will be considered for parallel algorithms. 

Your completed project must run and you must attend an interview with your tutor to receive a project grade. Bring a copy of this project sheet with you to the interview and place your name and id number in the space provided. Projects which do not run cannot receive more than 49% but may receive a much lower grade.

No individual projects will be accepted. Plagiarism is a serious offence and will be penalized as outlined in the Student handbook. 
Note: The map of Jamaica shown on page one was copied from -[http://www.orangesmile.com/common/img_country_maps/jamaica-map-1.jpg](http://www.orangesmile.com/common/img_country_maps/jamaica-map-1.jpg)


## Developers
JeVaughn Ferguson - da3ferg@gmail.com


Shanel Bailey - shanelbailey.sb@gmail


Leathon Gregory - leathongregory@gmail.com

## License
[MIT](https://choosealicense.com/licenses/mit/)