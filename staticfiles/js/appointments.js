$(function () {
    // page is now ready, initialize the calendar...
    const appointment = window.Appointment;
    appointment.initailizeCalender('appointments-calender-id');
});

(function (scope) {

    class Appointment {
        constructor ( ) {
            this.$calenderContainer = null;

            const modalElement = document.getElementById('create-event-id');
            this.createAppointmentModal = M.Modal.init(modalElement, {
                dismissible: true,
                onOpenStart: this.onModalStart
            });
        }

        initailizeCalender($id) {
            this.$calenderContainer = $(`#${$id}`);
            this.$calenderContainer.fullCalendar({
                events: [
                    {
                      title: 'My Event',
                      start: '2018-10-15'
                    }
                    // other events here
                ],
                height: '500',
                weekends: false,
                defaultView : "agendaWeek",
                displayEventTime:true,
                defaultTimedEventDuration:'00:30:00',
                displayEventEnd:true,
                eventDurationEditable: false,
                eventLimit: false,//show more than one event in the month view day
                selectable: true,
                selectHelper :true,
                selectOverlap :false,
                titleRangeSeparator : " - ",
                nowIndicator : true,    
                allDaySlot: true,
                slotDuration: '00:30:00',
                minTime: '10:00:00',
                maxTime: '17:00:00',
                forceEventDuration: true,   
                droppable: true, 
                header:{
                    left: 'month agendaWeek',
                    center: 'title',
                    right: 'today prev,next'
                },
                dayClick: date => this.handleDayClicked(date),
                eventClick: (calEvent, jsEvent, view) => this.handleEventClicked(calEvent, jsEvent, view),
                eventAfterAllRender: () => this.handleAfterAllRender(),
                eventRender : ( event, element, view ) => this.handleEventRender(event, element, view),
                eventDragStop: (seg, ev) => this.handleEventDragStop(seg),
                drop: (date) => this.handleDrop(date),
                select: (start, end, allDay) => this.handleDateSelect(start, end, allDay),
            });
        }

        handleDateSelect (start, end, allDay) {
            console.log('handleDateSelect');
            this.createAppointmentModal.open();
        }

        handleAfterAllRender () {
            $(".tooltip").tooltip();
        }
        
        handleEventDragStop (seg) {
            console.log('handleDayClicked');
        }
        
        handleDrop (date) {
        }

        handleDayClicked (date) {
            console.log('handleDayClicked');
            this.$calenderContainer.fullCalendar('gotoDate', date)
            this.$calenderContainer.fullCalendar('changeView', 'agendaWeek');
        }
        
        handleEventClicked (calEvent, jsEvent, view) {
            console.log('Event: ' + calEvent.title);
            console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            console.log('View: ' + view.name);
        
            // change the border color just for fun
            $(this).css('border-color', 'red');
        
        }

        handleEventRender(event, element, view) {
            // element.attr({  
            //     "data-tooltip": event.title,
            //     "data-position": "right",
            //     "data-delay":"10"
            // });
            // element.addClass("tooltip");
        }

        onModalStart() {
            console.log("Modal Open");
            
        }



    }

    scope.Appointment = new Appointment();
    
})(window);