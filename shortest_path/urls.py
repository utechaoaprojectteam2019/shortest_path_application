from django.urls import path, include
from django.conf.urls import url
from .views import (index_view, get_shortest_path, calc_path)

app_name = 'index'

urlpatterns = [
    path('', index_view, name='index'),
    path('get_shortest_path', get_shortest_path, name='get_shortest_path'),
    path('download_shortest_path', calc_path, name='download_shortest_path'),
]