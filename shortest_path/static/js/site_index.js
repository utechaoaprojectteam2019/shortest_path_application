function initializeMaps(){

	document.getElementById('googlemaps').style.backgroundColoe = "#ebe8de";

	 // The latitude and longitude of your business / place
	 var position = [18.0150705, -76.7634839];

    var latLng = new google.maps.LatLng(position[0], position[1]);

    var mapOptions = {
        zoom: 12, // initialize zoom level - the max value is 21
        streetViewControl: false, // hide the yellow Street View pegman
        scaleControl: false, // allow users to zoom the Google Map
        mapTypeId: google.maps.MapTypeId.hybrid,
        center: latLng,
        scrollwheel:false,
        disableDefaultUI: true,
        zoomControl: false,
        mapTypeControl: false,
        rotateControl: false,
        fullscreenControl: false
    };

    map = new google.maps.Map(document.getElementById('googlemaps'),mapOptions);

    console.log("init")
}


 $(document).on('submit', "#route_cost", function(e) {
    // Stop form from submitting normally
    e.preventDefault();

    var friendForm = $(this);
    // Send the data using post
    var posting = $.post( friendForm.attr('action'), friendForm.serialize() );
    // if success:


    posting.done(function(data) {
        var ol = document.createElement("ol");
        ol.setAttribute("class", "breadcrumb");

        var el = document.createElement("li");
        el.setAttribute("class", "breadcrumb-item active");
        el.innerHTML = "ROUTE PATH ";
        ol.appendChild(el);
        for (var i=0;  i < data['dijkstra_path'].length ; i++) {
            var el = document.createElement("li");
            el.setAttribute("class", "breadcrumb-item");
            el.innerHTML = data['dijkstra_path'][i];
            ol.appendChild(el);
        }   

        $("#results").html(ol)
        $("#sub_graph").attr("src", data['sub_graph']);
        $("#results_cost").html("Route Cost " + "<span class='badge badge-primary'>" + data['dijkstra_path_length'] + "</span>")
    });
    // if failure:
    posting.fail(function(data) {
        // 4xx or 5xx response, alert user about failure
    });
});


window.onload = function(){initializeMaps()};