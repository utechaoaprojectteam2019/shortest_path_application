from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.conf import settings
from django.http import HttpResponse, JsonResponse
import io
import pandas
import networkx as nx


cities = ["Annotto Bay", "Black River",
          "Christiana","Falmouth","KINGSTON","Lucea","Mandeville","May Pen",
          "Montego Bay","Morant Bay","Ocho Rios","Port Antonio","Port Maria",
          "St. Anns Bay","Savanna-La-Mar","Spanish Town"]

# Function based views for views

def index_view(request):
	filename = settings.DOCUMENT_PATH
	df = pandas.read_excel(filename)
	str_io = io.StringIO()
	df.to_html(buf=str_io, classes='table table-striped table-bordered table-hover text-nowrap shortest_path_table', justify='initial', border=0)
	html_str = str_io.getvalue()
	return render(request, 'shortest_path/index.html', {"shortest_path_table" : html_str, "towns": list(df['Towns']) })

@require_http_methods(["POST"])
def get_shortest_path(request):

	# prepare graph of town
	G = prepareGraph();

	source = request.POST['source']
	destination = request.POST['destination']

	source = source.lower()
	destination = destination.lower()

	dijkstra_path = nx.dijkstra_path(G, source, destination)
	dijkstra_path_length = nx.dijkstra_path_length(G, source, destination)

	return JsonResponse({'dijkstra_path': dijkstra_path,'dijkstra_path_length': dijkstra_path_length})

@require_http_methods(["POST"])
def calc_path(request):

	filename = "dijkstra_path_shortest.xlsx"

	G = prepareGraph()
	df = prepareDf()
	
	data = []
	towns = sorted(list(set(df["Town"])))

	for t in df['Town']:
		row = []
		for r in df['Town']:
		    if nx.has_path(G, t.lower(), r.lower()):
		        row.append(nx.dijkstra_path_length(G, t.lower(), r.lower()))
		data.append(row)
	shortDistanceDf = pandas.DataFrame(data, columns=df['Town'], index=towns)

	workbook = io.BytesIO()
	PandasWriter = pandas.ExcelWriter(workbook, engine='xlsxwriter')
	shortDistanceDf.to_excel(PandasWriter, sheet_name='Shortest Distance')
	PandasWriter.save()
	PandasWriter.close()

	workbook.seek(0)

	response = HttpResponse(workbook.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
	response['Content-Disposition'] = f'attachment; filename={filename}'

	return response


def prepareGraph():	

	df = prepareDf()

	# create graph
	G = nx.Graph()

	# add nodes

	G.add_nodes_from(df['Town'].str.lower())

	# create edges

	graph = []
	j=0
	l=0
	for i, t in df.iterrows():
		if(j >= len(cities)):
			j = 0
		for city in cities:
			graph.append((t['Town'].lower(),city.lower(),int(t['d'+str(j)])))
			j+=1

	# add egdes

	G.add_weighted_edges_from( graph )

	return G

def prepareDf():
	# read the cities and distance from 17 towns

	filename = settings.CITIES_AND_TOWN
	df = pandas.read_excel(filename)

	return df



